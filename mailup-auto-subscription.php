<?php
/**
 * Plugin Name: MailUp Auto Subscription
 * Plugin URI: https://www.ilghera.com/product/mailup-auto-subscription/
 * Description: Let users subscribe to MailUp newsletter service in the same time they're registering to your site.
 * Author: ilGhera
 * Version: 1.2.0
 * Author URI: http://ilghera.com
 * Text Domain: mailup-auto-subscription
 * Domain Path: /languages/
 * Requires at least: 4.0
 * Tested up to: 6.7
 *
 * @package mailup-auto-subscription
 */

/**
 * Internationalization
 *
 * @return void
 */
function mas_load_textdomain() {

	load_plugin_textdomain( 'mailup-auto-subscription', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'mas_load_textdomain' );

/**
 * Add menu
 *
 * @return void
 */
function mas_add_menu() {

	add_menu_page( 'MailUp Auto Subscription', 'MailUp A.S.', 'manage_options', 'mailup-auto-subscription', 'mas_options', 'dashicons-email' );
}
add_action( 'admin_menu', 'mas_add_menu' );

/**
 * Enqueue scripts and styles
 *
 * @return void
 */
function mas_enqueue_scripts() {

    $screen = get_current_screen();

    if ( 'toplevel_page_mailup-auto-subscription' === $screen->id ) {

        define( 'MAS_VERSION', '1.2.0' );

        /* css */
        wp_enqueue_style( 'tzcheckbox-style', plugin_dir_url( __FILE__ ) . 'js/tzCheckbox/jquery.tzCheckbox/jquery.tzCheckbox.css', array(), MAS_VERSION );

        /* js */
        wp_enqueue_script( 'tzcheckbox', plugin_dir_url( __FILE__ ) . 'js/tzCheckbox/jquery.tzCheckbox/jquery.tzCheckbox.js', array( 'jquery' ), MAS_VERSION, false );
        wp_enqueue_script( 'tzcheckbox-script', plugin_dir_url( __FILE__ ) . 'js/tzCheckbox/js/script.js', array( 'jquery' ), MAS_VERSION, false );
    }
}
add_action( 'admin_enqueue_scripts', 'mas_enqueue_scripts' );

/**
 * Options page
 *
 * @return void
 */
function mas_options() {

	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( esc_html__( 'It looks like you do not have sufficient permissions to view this page.', 'mailup-auto-subscription' ) );
	}

	/* Page template */
	echo '<div class="wrap">';
	echo '<div class="wrap-left" style="float:left; width:70%;">';

	echo '<h1>MailUp Auto Subscription</h1>';
	echo '<p>' . esc_html__( 'Register your users to MailUp in the same time they subscribe to your site.', 'mailup-auto-subscription' ) . '<br>' .
	esc_html__( 'Enter the Host of your console, choose the list and if necessary the group created in MailUp.', 'mailup-auto-subscription' ) . '<br></p>';

	$host       = get_option( 'mas-host' );
	$list       = get_option( 'mas-list' );
	$group      = get_option( 'mas-group' );
	$confirm    = get_option( 'mas-confirm' );
	$newsletter = get_option( 'mas-newsletter' );

	if ( isset( $_POST['mas-admin-settings'], $_POST['mas-admin-options-nonce'] ) && wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['mas-admin-options-nonce'] ) ), 'mas-admin-options' ) ) {

		if ( isset( $_POST['host'] ) ) {
			$host = trim( sanitize_text_field( wp_unslash( $_POST['host'] ) ) );
			update_option( 'mas-host', $host );
		}

		if ( isset( $_POST['list'] ) ) {
			$list = trim( sanitize_text_field( wp_unslash( $_POST['list'] ) ) );
			update_option( 'mas-list', $list );
		}

		if ( isset( $_POST['group'] ) ) {
			$group = trim( sanitize_text_field( wp_unslash( $_POST['group'] ) ) );
			update_option( 'mas-group', $group );
		}

		if ( ! get_option( 'mas-confirm' ) ) {
			add_option( 'mas-confirm', 'true' );
		}

		if ( isset( $_POST['sent'] ) && 'true' === sanitize_text_field( wp_unslash( $_POST['sent'] ) ) ) {
			$confirm = ( isset( $_POST['confirm'] ) && 'true' === sanitize_text_field( wp_unslash( $_POST['confirm'] ) ) ) ? 'true' : 'false';
			update_option( 'mas-confirm', $confirm );
		}

		if ( ! get_option( 'mas-newsletter' ) ) {
			add_option( 'mas-newsletter', 'true' );
		}

		if ( isset( $_POST['sent'] ) && 'true' === sanitize_text_field( wp_unslash( $_POST['sent'] ) ) ) {
			$newsletter = ( isset( $_POST['newsletter'] ) && 'true' === sanitize_text_field( wp_unslash( $_POST['newsletter'] ) ) ) ? 'true' : 'false';
			update_option( 'mas-newsletter', $newsletter );
		}
	}

	/* Displayed with both options disabled */
	if ( 'false' === get_option( 'mas-confirm' ) && 'false' === get_option( 'mas-newsletter' ) ) {
		echo '<div class="error"><p><strong>' .

		esc_html__( '<strong>ATTENTION!</strong> Sending newsletter without user confirmation is considered spam.', 'mailup-auto-subscription' )

		. '</p></div>';
	}

	/* Include the admin form */
	include plugin_dir_path( __FILE__ ) . '/includes/mas-admin-form.php';

	echo '</div>'; // Wrap left.
	echo '<div class="wrap-right" style="float:left; width:30%; text-align:center; padding-top:3rem;">';
	echo '<iframe width="300" height="800" scrolling="no" src="http://www.ilghera.com/images/mas-iframe.html"></iframe>';
	echo '</div>';
	echo '<div class="clear"></div>';
	echo '</div>';
}

if ( 'true' === get_option( 'mas-newsletter' ) ) {

	/* Modify the registration form */
	include plugin_dir_path( __FILE__ ) . '/includes/mas-register-form.php';
}

/**
 * Registration function
 *
 * @param int $user_id the user ID.
 *
 * @return void
 */
function mas_mailup_register( $user_id ) {

	$user_info    = get_userdata( $user_id );
	$mas_username = $user_info->user_login;
	$mas_mail     = $user_info->user_email;
	$mas_host     = get_option( 'mas-host' );
	$mas_list     = get_option( 'mas-list' );
	$mas_group    = get_option( 'mas-group' );
	$mas_confirm  = get_option( 'mas-confirm' );

	/* With registration form option activated */
	if ( 'true' === get_option( 'mas-newsletter' ) ) {

		if ( isset( $_POST['mas-mailup-registration-nonce'] ) && wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['mas-mailup-registration-nonce'] ) ), 'mas-mailup-registration' ) ) {

			$mas_user_newsletter = ( isset( $_POST['user-newsletter'] ) && 'true' === sanitize_text_field( wp_unslash( $_POST['user-newsletter'] ) ) ) ? 'true' : 'false';

			if ( 'false' !== $mas_user_newsletter ) {

				$url = "$mas_host/frontend/xmlSubscribe.aspx?list=$mas_list&group=$mas_group&email=$mas_mail&confirm=$mas_confirm&csvFldNames=campo1&csvFldValues=$mas_username";

				wp_remote_post( $url );

			}
		}
	} else {

		$url = "$mas_host/frontend/xmlSubscribe.aspx?list=$mas_list&group=$mas_group&email=$mas_mail&confirm=$mas_confirm&csvFldNames=campo1&csvFldValues=$mas_username";

		wp_remote_post( $url );

	}

}
add_action( 'user_register', 'mas_mailup_register' );

