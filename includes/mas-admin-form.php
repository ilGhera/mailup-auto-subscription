<?php
/**
 * Admin form
 *
 * @author ilGhera
 * @package mailup-auto-subscription/includes
 *
 * @since 1.2.0
 */

echo '<form name="host" id="host" method="post" action="">';
echo '<table class="form-table"><tbody>';

echo '<tr>';
echo '<th scope="row">' . esc_html__( 'Endpoint/ Console url', 'mailup-auto-subscription' ) . '</th>';
echo '<td>';
echo '<input type="text" name="host" id="host" value="' . esc_html( $host ) . '">';
echo '<p class="description">';

echo sprintf(
	/* Translators: the URL */
	esc_html__( 'Do you need help? Click %s', 'mailup-auto-subscription' ),
	'<a href="http://help.mailup.com/display/mailupapi/MailUp+API+Credentials" target="_blank">here</a>'
);
echo '</p>';

echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th scope="row">' . esc_html__( 'MailUp List ID', 'mailup-auto-subscription' ) . '</th>';
echo '<td>';
echo '<input type="text" name="list" id="list" value="' . intval( $list ) . '">';
echo '<p class="description">' . esc_html__( 'Enter the ID of the list which you want to register your users.', 'mailup-auto-subscription' ) . '</strong></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th scope="row">' . esc_html__( 'MailUp Group ID', 'mailup-auto-subscription' ) . '</th>';
echo '<td>';
echo '<input type="text" name="group" id="group" value="' . intval( $group ) . '">';
echo '<p class="description">' . esc_html__( 'If you want to add users to a specific MailUp group, enter here the correct ID.', 'mailup-auto-subscription' ) . '</strong></p>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<th scope="row">' . esc_html__( 'Confirm', 'mailup-auto-subscription' ) . '</th>';
echo '<td>';
echo '<input type="hidden" name="sent" id="sent" value="true">';
echo '<input type="checkbox" name="confirm" id="confirm" value="true"';

if ( 'true' === $confirm ) {
	echo 'checked="checked"';
}

echo '>';
echo '<p class="description">' . esc_html__( 'Send MailUp register confirmation', 'mailup-auto-subscription' ) . '</p>';
echo '</td>';
echo '</tr>';


echo '<tr>';
echo '<th scope="row">' . esc_html__( 'Registration form', 'mailup-auto-subscription' ) . '</th>';
echo '<td>';
echo '<input type="hidden" name="sent" id="sent" value="true">';
echo '<input type="checkbox" name="newsletter" id="newsletter" value="true"';

if ( 'true' === $newsletter ) {
	echo 'checked="checked"';
}

echo '>';
echo '<p class="description">' . esc_html__( 'Newsletter option in the registration form', 'mailup-auto-subscription' ) . '</p>';
echo '</td>';
echo '</tr>';

echo '</table>';
echo '</tbody>';

wp_nonce_field( 'mas-admin-options', 'mas-admin-options-nonce' );

echo '<p class="submit"><input name="mas-admin-settings" class="button button-primary" type="submit" value="' . esc_html__( 'Save changes', 'mailup-auto-subscription' ) . '"></p>';

echo '</form>';

