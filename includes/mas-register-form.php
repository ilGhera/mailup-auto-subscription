<?php
/**
 * Extra field login form
 *
 * @author ilGhera
 * @package mailup-auto-subscription/includes
 *
 * @since 1.2.0
 */

/**
 * Extra field login form function
 *
 * @return void
 */
function mas_add_field() {
	?>
	<p style="margin-bottom:10px;">
	<label style="text-align: left;">
	<input style="margin-top: 0;" type="checkbox" name="user-newsletter" id="user-newsletter" value="true" checked="checked"/>
	<?php esc_html_e( 'Newsletter subscriptions', 'mailup-auto-subscription' ); ?>
	</label>
	</p>
	<?php wp_nonce_field( 'mas-mailup-registration', 'mas-mailup-registration-nonce' ); ?>
	<?php
}
add_action( 'register_form', 'mas_add_field' );

