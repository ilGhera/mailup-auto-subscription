��          �            x  \   y     �     �  ]   �  ?   W  O   �  G   �     /     ?  *   N     y  K   �     �     �  !   �  �    e   �     W     `  _     >   �  a     N   �     �     �  ,   �       T   3     �     �  0   �     	       
                                                                   <strong>ATTENTION!</strong> Sending newsletter without user confirmation is considered spam. Confirm Do you need help? Click %s Enter the Host of your console, choose the list and if necessary the group created in MailUp. Enter the ID of the list which you want to register your users. If you want to add users to a specific MailUp group, enter here the correct ID. It looks like you do not have sufficient permissions to view this page. MailUp Group ID MailUp List ID Newsletter option in the registration form Newsletter subscriptions Register your users to MailUp in the same time they subscribe to your site. Registration form Save changes Send MailUp register confirmation Project-Id-Version: MailUp Auto Subscription
PO-Revision-Date: 2025-01-27 12:58+0100
Last-Translator: 
Language-Team: ilGhera <andrea@ilghera.com>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.5
X-Poedit-KeywordsList: __;gettext;gettext_noop;_;esc_html__;esc_html_e
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 <strong>ATTENZIONE!</strong> L'invio di newsletter senza il consenso dell'utente è considerato spam. Conferma Hai bisogno d'aiuto? Clicca %s Inserisci l'Host della tua console, scegli la lista e se necessario il gruppo creato in MailUp. Inserisci l'ID della lista a cui vuoi iscrivere i tuoi utenti. Se vuoi aggiungere gli utenti ad un gruppo di destinatari specifico, inserisci qui l'ID corretto. Sembra che tu non abbia i permessi sufficienti per visualizzare questa pagina. ID gruppo MailUp ID lista MailUp Opzione Newsletter nel form di registrazione Iscrizione Newsletter Iscrivi i tuoi utenti a MailUp nello stesso momento in cui si iscrivono al tuo sito. Form di registrazione Salva le modifiche Invia richiesta di conferma iscrizione di MailUp 